Llanta w1,w2;
Carro c1;
float angulo;
float x;
void setup(){
   size(800,400);
   w1 = new Llanta(width/2, height/2, 30);
   w2 = new Llanta(width/2-90, height/2, 30);
   c1 = new Carro(w1,w2);
   angulo = 0;
   x = -w1.radio;
}
void draw(){
   background(180);

   pushMatrix();
       c1.dibujaCarro(angulo, x, height/2);
   popMatrix();

   angulo += 0.05;
   stroke(255);
   if( x < (width + 6*w1.radio)  )
       x += 2;
   else
       x = -2*w1.radio;
   line( 0, height/2+w1.radio+1  , width  ,height/2+w1.radio+1);
}
