class Llanta{
   float x, y;
   float radio;
   Llanta(float a, float b, float c){
       x = a;
       y = b;
       radio = c;
       ellipseMode(CENTER);
   }   
   void dibujaLlanta(){
      fill(0);
      strokeWeight(1);
      ellipse(x,y,2*radio, 2*radio);
      pushMatrix();
           fill(255);
          translate(x,y);
          rect(0, -(radio-5), 5,5 );
          fill(180);
          ellipse(0,0,20,20);
      popMatrix();
   }
}
