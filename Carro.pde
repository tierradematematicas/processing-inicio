class Carro{
    Llanta w1;
    Llanta w2;
    
    public Carro(Llanta a, Llanta b){
         w1 = a;
         w2 = b;
    }
    
    void dibujaCarro(float angulo,float x, float altura){
    pushMatrix();
       translate(x, altura);
       strokeWeight(3);
       stroke(0);
       // DEFENSA DELANTERA
       line(w1.radio,0, 2*w1.radio ,0);
       line(2*w1.radio,-w1.radio-10, 2*w1.radio ,0);
       rotate(angulo);
       w1.x=0;
       w1.y=0;
       w1.dibujaLlanta();
   popMatrix();
   stroke(0);
   strokeWeight(3);
   
   pushMatrix();
       translate(x-3*w1.radio, altura);
       strokeWeight(3);
       line(w2.radio,0, 2*w2.radio ,0);
       //DEFENSA TRASERA
       line(-w2.radio,0, -2*w2.radio ,0);
       line(-2*w2.radio,-w2.radio-10, -2*w2.radio ,0);
       rotate(angulo);
       w2.x=0;
       w2.y=0;
       w2.dibujaLlanta();
   popMatrix();
   
    }
    
}
