**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

Programa que muestra como crear una clase y usar objetos a partir de esta en un sketch.

---

## Crea una clase

Si observa, verá que están las clases Llanta y Carro, están se relacionan entre si para crear objetos tipo Carro.


---

## Crea el sketch

El código del sketch muestra como crear objetos y ponerlos en la ventana.

Puede ver además el uso de los métodos pushMatrix() y popMatrix().

---
